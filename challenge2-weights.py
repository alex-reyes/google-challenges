def get_weights(max_weight):
    """
    Assuming that the largest weight needed to balance the scale is
    strictly less heavy than twice the starting weight on the left,
    get the set of weights that may be needed to balance the scale
    given some max_weight.
    """
    result = []
    weight_number = 0
    while (3 ** weight_number) < (2 * max_weight):
        result.append(3 ** weight_number)
        weight_number += 1
    return result

MAX_WEIGHT = 1000000000
WEIGHTS = get_weights(MAX_WEIGHT)

# lookup table for recursive answer function (DYNAMIC PROGRAMMING™)
CACHED_RESULTS = {
    0: ['-'],
    1: ['R']
}

# used by "invert" function when a weight heavier than the
# starting weight is used as a counter-balance
INVERSION_MAP = {
    'L': 'R',
    'R': 'L',
    '-': '-'
}


def invert(l):
    """
    Given some configuration of weights, return its inverse
    ('L' -> 'R', 'R' -> 'L', '-' -> '-')
    e.g.: invert(['R', 'L', '']) == ['L', 'R', '-'])
    """
    return [INVERSION_MAP[i] for i in l]


def answer(x):
    if x < 0:
        return invert(answer(abs(x)))
    if x in CACHED_RESULTS:
        return CACHED_RESULTS[x]

    weights_possibly_used = [weight for weight in WEIGHTS if weight < (2 * x)]
    len_result = len(weights_possibly_used)

    if weights_possibly_used[-1] > x:
        sub_result = invert(answer(weights_possibly_used[-1] - x))
    else:
        sub_result = answer(x - weights_possibly_used[-1])

    result = sub_result + ['-'] * (len_result - len(sub_result) - 1) + ['R']
    CACHED_RESULTS[x] = result
    return result
