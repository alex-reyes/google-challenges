from collections import defaultdict, deque


def answer(document, searchTerms):
    doc_as_list = document.split(' ')
    search_terms_set = set(searchTerms)

    # build dictionary of lists of in-order word locations
    term_locations = defaultdict(deque)
    for idx, word in enumerate(doc_as_list):
        if word in search_terms_set:
            term_locations[word].append(idx)

    # get earliest first occurence and latest last occurence of terms
    #  - this will be the longest snippet, which we'll then trim
    snippet_start = min(term_locations[term][0] for term in term_locations)
    snippet_end = max(term_locations[term][-1] for term in term_locations)

    # build snippet
    snippet_as_deque = deque(doc_as_list[snippet_start:snippet_end + 1])

    trimmed = True
    while trimmed:
        # check how many words could be trimmed from left
        # without removing the all instances of a search term
        leftmost_word = snippet_as_deque[0]
        num_trimmable_from_left = 0
        words_trimmed_from_left = defaultdict(int)
        if len(term_locations[leftmost_word]) > 1:
            for word in snippet_as_deque:
                if word not in term_locations or (len(term_locations[word]) - words_trimmed_from_left[word]) > 1:
                    words_trimmed_from_left[word] += 1
                    num_trimmable_from_left += 1
                else:
                    break

        # check how many words could be trimmed from right
        rightmost_word = snippet_as_deque[-1]
        num_trimmable_from_right = 0
        words_trimmed_from_right = defaultdict(int)
        if len(term_locations[rightmost_word]) > 1:
            for word in reversed(snippet_as_deque):
                if word not in term_locations or (len(term_locations[word]) - words_trimmed_from_right[word]) > 1:
                    words_trimmed_from_right[word] += 1
                    num_trimmable_from_right += 1
                else:
                    break

        if num_trimmable_from_right == 0 and num_trimmable_from_left == 0:
            trimmed = False
        elif num_trimmable_from_left > num_trimmable_from_right:
            for _ in range(num_trimmable_from_left):
                word = snippet_as_deque.popleft()
                if word in term_locations:
                    term_locations[word].popleft()
            trimmed = True
        else:
            for _ in range(num_trimmable_from_right):
                word = snippet_as_deque.pop()
                if word in term_locations:
                    term_locations[word].pop()
            trimmed = True

    return ' '.join(snippet_as_deque)


if __name__ == '__main__':
    document = "many google employees can program"
    searchTerms = ["google", "program"]
    document = "a b c d a"
    searchTerms = ["a", "c", "d"]
    document = "world there hello hello where world"
    searchTerms = ["hello", "world"]

    snippet = answer(document, searchTerms)
    print(snippet)
    # assert snippet == 'c d a'
