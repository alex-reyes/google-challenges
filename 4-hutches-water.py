import time
from collections import defaultdict


class Grid(object):
    def __init__(self, heights):
        self.heights = list(heights)
        self.dxs_by_height = defaultdict(list)
        self.max_height = 1
        for dx, height in enumerate(self.heights):
            self.dxs_by_height[height].append(dx)
            self.max_height = max(self.max_height, height)
        self.leftmost_hutch = self.dxs_by_height[self.max_height][0]
        self.rightmost_hutch = self.dxs_by_height[self.max_height][-1]
        self.width = len(heights)
        self.water_cells_count = 0
        self.done_processing = False
        self.num_columns_above = 0

    def process_row(self, dy):
        hutch_dxs = self.dxs_by_height[dy + 1]
        if not hutch_dxs and not self.num_columns_above:
            return
        if hutch_dxs:
            self.leftmost_hutch = min(hutch_dxs[0], self.leftmost_hutch)
            self.rightmost_hutch = max(hutch_dxs[-1], self.rightmost_hutch)
        hutch_distance = self.rightmost_hutch - self.leftmost_hutch
        if hutch_distance > 1:
            add_water_cells = hutch_distance + 1 - self.num_columns_above - len(hutch_dxs)
            self.water_cells_count += add_water_cells
        self.num_columns_above += len(hutch_dxs)


def answer(heights, print_grid=False, return_grid=False):
    grid = Grid(heights)

    # fill with water and count water
    # t0 = time.clock()  # devcomment
    # rows_per_print = 90  # devcomment
    for dy in range(grid.max_height - 1, 0, -1):
        # if not dy % rows_per_print:  # devcomment
            # dt = time.clock() - t0  # devcomment
            # print('at row %s after %s (%s per row)' % (dy, dt, dt / float(grid.max_height - dy)))  # devcomment
        grid.process_row(dy)
        if grid.done_processing:
            break
    if print_grid:
        grid.print_grid()

    result = grid.water_cells_count
    if return_grid:
        return result, grid
    else:
        return result


if __name__ == '__main__':
    # heights = [1, 4, 2, 5, 1, 2, 3]
    import random;  heights = [random.randint(1, 9000) for _ in range(100000)]
    count = answer(heights)
    print('answer is %s' % count)
    # assert count == 5

# ...X...
# .X.X...
# .X.X..X
# .XXX.XX
# XXXXXXX
# 1425123
