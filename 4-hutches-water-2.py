from collections import defaultdict


def answer(heights):
    """
    For a given height, all empty cells with hutches to
    both the left and right will fill with water, because
    the hutch columns go all the way down. The number of
    empty cells is equal to the number of cells between
    the left- and right-most hutches, minus the number of
    columns with peaks at or above that height.
    """
    # organize peaks by height for top-down approach
    #   + grab max_height while we're at it
    peaks_by_height = defaultdict(list)
    max_height = 1
    for idx, height in enumerate(heights):
        peaks_by_height[height].append(idx)
        max_height = max(max_height, height)

    # initialize counts and hutch bounds
    water_cells_count = 0
    num_peaks_above = 0
    leftmost_hutch = peaks_by_height[max_height][0]
    rightmost_hutch = peaks_by_height[max_height][-1]

    # calculate empty & bounded cells at each height
    for height in range(max_height, 1, -1):
        peaks_at_height = peaks_by_height[height]
        # if there are new peak(s), the bounds have changed
        if peaks_at_height:
            leftmost_hutch = min(peaks_at_height[0], leftmost_hutch)
            rightmost_hutch = max(peaks_at_height[-1], rightmost_hutch)
        hutch_distance = rightmost_hutch - leftmost_hutch

        # if one or more cells between bounding cells,
        #   calculate how many are empty and add to
        #   water cell count
        if hutch_distance > 1:
            add_water_cells = hutch_distance + 1 - num_peaks_above - len(peaks_at_height)
            water_cells_count += add_water_cells

        # add peaks from this height to total above for next height
        num_peaks_above += len(peaks_at_height)

    return water_cells_count


if __name__ == '__main__':
    # heights = [1, 4, 2, 5, 1, 2, 3]
    import random;  heights = [random.randint(1, 9000) for _ in range(100000)]
    count = answer(heights)
    print('answer is %s' % count)
    # assert count == 5
