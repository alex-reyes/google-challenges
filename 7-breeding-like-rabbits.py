class Population(object):
    def __init__(self):
        self.calculations = 0
        self.population_dict = {
            0: 1,
            1: 1,
            2: 2,
        }

    def __call__(self, n):
        return self.get_population(n)

    def get_population(self, t):
        try:
            return self.population_dict[t]
        except KeyError:
            self.calculations += 1
            return self.calculate_population(t)

    def calculate_population(self, t):
        R = self.get_population
        if t % 2:
            n = (t - 1) / 2
            result = R(n - 1) + R(n) + 1
        else:
            n = t / 2
            result = R(n) + R(n + 1) + n
        self.population_dict[t] = result
        return result


def binary_search(p, S, left, right, parity):
    while (right - left) > 2:
        proto_guess = left + (right - left) / 2
        guess = proto_guess - (proto_guess % 2) + parity
        p_guess = p(guess)
        if p_guess == S:
            left = right = guess
        elif S > p_guess:
            left = guess
        else:
            right = guess

    if p(right) == S:
        result = right
    elif p(left) == S:
        result = left
    else:
        result = -1
    return result


def get_pow2_bounds(p, S, parity):
    """
    find consecutive powers of 2 (minus 1, depending on parity)
    'left' and 'right' such that p(left) <= S <= p(right)
    """
    power = 0
    left = 0
    population = 1
    while population < S:
        n = pow(2, power) - parity
        population = p(n)
        if population <= S:
            left = n
        right = n
        power += 1

    return left, right


def answer(str_S):
    p = Population()
    S = int(str_S)
    output = -1

    for parity in (0, 1):
        left, right = get_pow2_bounds(p, S, parity)
        result = binary_search(p, S, left, right, parity)
        output = max(result, output)

    output = str(output) if output > 0 else 'None'
    return output


def answer_slow(str_S, p=None):
    if p is None:
        p = Population()

    S = int(str_S)
    n = 1
    continue_even = continue_odd = True
    results = []
    while continue_even or continue_odd:
        t_even = 2 * n
        t_odd = t_even + 1

        if continue_even:
            p_even = p(t_even)
            if p_even >= S:
                continue_even = False
            if p_even == S:
                results.append(t_even)

        if continue_odd:
            p_odd = p(t_odd)
            if p_odd >= S:
                continue_odd = False
            if p_odd == S:
                results.append(t_odd)

        n += 1

    output = str(max(results)) if results else 'None'
    return output


def main():
    test_cases = (
        # (input, correct output)
        ('7', '4'),
        ('100', 'None'),
        ('10003', '2602'),
        (str(10**10), 'None'),
        (str(10**15), 'None'),
        (str(10**16), 'None'),
        (str(10**17), 'None'),
        (str(10**18), 'None'),
        (str(10**19), '654554755662530561'),
        (str(10**20), '6024402318631079566'),
        (str(10**25), 'None'),
    )

    successful_tests = 0
    for test_case in test_cases:
        str_S, correct_output = test_case
        output = answer(str_S)
        if output == correct_output:
            successful_tests += 1
        else:
            print('failed test %s (output: %s)' % (str(test_case), output))

    print('%s of %s tests successful' % (successful_tests, len(test_cases)))


if __name__ == '__main__':
    main()
    # for str_S in map(str, range(100)):
    #     a = answer(str_S)
    #     a_slow = answer_slow(str_S)
    #     if a != a_slow:
    #         print('failed on %s; answer: %s, answer_slow: %s' % (str_S, a, a_slow))
    # p = Population()
    # print(list((answer(s), answer_slow(s)) for s in map(str, range(10))))
