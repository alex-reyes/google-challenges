def answer(x):
    distinct_codes = set()
    for code in x:
        if (code not in distinct_codes) and (code[::-1] not in distinct_codes):
            distinct_codes.add(code)
    return len(distinct_codes)

if __name__ == '__main__':
    assert answer(["foo", "bar", "oof", "bar"]) == 2
    assert answer(["x", "y", "xy", "yy", "", "yx"]) == 5
    print('tests passed')
