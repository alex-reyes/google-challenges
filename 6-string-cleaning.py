from functools import wraps


def get_occurrences(chunk, word):
    """
    get first index of all occurrences of word in chunk
    """
    occurrences = []
    for i in range(len(chunk)):
        if chunk[i:i + len(word)] == word:
            occurrences.append(i)
    return occurrences


def remove_occurrence(chunk, word, i):
    """
    remove occurrence of word at i
    note: doesn't verify that word is actually there
    """
    return chunk[:i] + chunk[i + len(word):]


def better_result(a, b):
    """
    returns shorter string or, if length is equal, lexicographically
    earlier string
    """
    result = a if len(a) < len(b) or (len(a) == len(b) and a < b) else b
    return result


def cache_best(func):
    """
    memoize best result for given (sub)chunk and word, reducing the
    hurt from depth-first search with many insertions
    """
    cache = {}

    @wraps(func)
    def wrapper(chunk, word, best):
        if (chunk, word) in cache:
            return better_result(cache[(chunk, word)], best)
        else:
            result = func(chunk, word, best)
            cache[(chunk, word)] = result
            return result
    return wrapper


@cache_best
def answer_inner(chunk, word, best):
    """
    recursive depth-first search, keeping track of the current 'best'
    result found (lexicographically earliest shortest string)
    """
    if word not in chunk:
        return better_result(best, chunk)
    else:
        occurrences = get_occurrences(chunk, word)
        for i in occurrences:
            modified = remove_occurrence(chunk, word, i)
            best = answer_inner(modified, word, best)
            if best == '':
                break
        return best


def answer(chunk, word):
    result = answer_inner(chunk, word, best=chunk)
    return result


def answer_inner_dfs(chunk, word):
    if word not in chunk:
        result = [chunk]
    else:
        result = []
        occurrences = get_occurrences(chunk, word)
        for i in occurrences:
            modified_chunk = remove_occurrence(chunk, word, i)
            result.extend(answer_inner_dfs(modified_chunk, word))
    return result


if __name__ == '__main__':
    test_cases = [
        ("lololololo", "lol", "looo"),
        ("ololololol", "olo", "llol"),
        ("goodgooogoogfogoood", "goo", "dogfood"),
        ("ddddddd", "d", ""),
        ("aabb", "ab", ""),
        ("lollollol", "lol", ""),
        ("d", "b", "d"),
        ("lollolollolollol", "lol", "o"),
        ("d", "d", ""),
        ("aabababaabbaabababababbabaabbababb", "ab", ""),
        ("lolllolollololollolllololololllololol", "lol", "looo"),
        ("llolololl", "lol", ""),
        ("looloollool", "lool", "loo"),
    ]

    successful = 0
    for test_case in test_cases:
        chunk, word, correct_result = test_case
        print('testing case %s' % str(test_case))
        result = answer(chunk, word)
        if result == correct_result:
            successful += 1
        else:
            print('failed (result "%s" != "%s")' % (result, correct_result))
            print('failed test specs\n\tchunk: %s\n\tword: %s' % (chunk, word))
    print('%s out of %s test cases successful' % (successful, len(test_cases)))
